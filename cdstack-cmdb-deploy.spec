Name:       {{{ git_name }}}
Version:    {{{ git_version }}}
Release:    1%{?dist}
Summary:    cdstack-cmdb-deploy

License:    LGPLv3
URL:        https://bitbucket.org/code-orange/cdstack-cmdb-deploy
VCS:        {{{ git_vcs }}}

Source0: files/usr/sbin/cmdb-deploy
Source1: files/etc/cmdb/cmdb_deploy.conf
Source2: files/etc/cron.d/codeorange-cmdb

BuildArch: noarch

%description
cdstack-cmdb-deploy

%install
mkdir -p %{buildroot}/%{_bindir}
install -p -m 755 %{SOURCE0} %{buildroot}/%{_sbindir}
install -p -m 755 %{SOURCE1} %{buildroot}/%{_sysconfdir}/cmdb
install -p -m 755 %{SOURCE2} %{buildroot}/%{_sysconfdir}/cron.d
  
%files
%{_sbindir}/cmdb-deploy
%{_sysconfdir}/cmdb/cmdb_deploy.conf
%{_sysconfdir}/cron.d/codeorange-cmdb

%changelog
{{{ git_changelog }}}
